﻿using System;

namespace Coding2.__Lorredo_BSIT1C
{
	class Program
	{
		public static void Main(string[] args)
		{
			char letter;
			Console.Write("Enter a Letter: ");
			letter = Convert.ToChar(Console.ReadLine());
			
			DetermineifVowelorConsonant(letter);
			
			Console.ReadKey();
		}
		
		public static char DetermineifVowelorConsonant(char letter)
		{
			
			switch(letter)
			{
				case 'a':
				Console.WriteLine("VOWEL");
				break;
				case 'b':
				Console.WriteLine("CONSONANT");
				break;
				case 'c':
				Console.WriteLine("CONSONANT");
				break;
				case 'd':
				Console.WriteLine("CONSONANT");
				break;
				case 'e':
				Console.WriteLine("VOWEL");
				break;
				case 'f':
				Console.WriteLine("CONSONANT");
				break;
				case 'g':
				Console.WriteLine("CONSONANT");
				break;
				case 'h':
				Console.WriteLine("CONSONANT");
				break;
				case 'i':
				Console.WriteLine("VOWEL");
				break;
				case 'j':
				Console.WriteLine("CONSONANT");
				break;
				case 'k':
				Console.WriteLine("CONSONANT");
				break;
				case 'l':
				Console.WriteLine("CONSONANT");
				break;
				case 'm':
				Console.WriteLine("CONSONANT");
				break;
				case 'n':
				Console.WriteLine("CONSONANT");
				break;
				case 'o':
				Console.WriteLine("VOWEL");
				break;
				case 'p':
				Console.WriteLine("CONSONANT");
				break;
				case 'q':
				Console.WriteLine("CONSONANT");
				break;
				case 'r':
				Console.WriteLine("CONSONANT");
				break;
				case 's':
				Console.WriteLine("CONSONANT");
				break;
				case 't':
				Console.WriteLine("CONSONANT");
				break;
				case 'u':
				Console.WriteLine("VOWEL");
				break;
				case 'v':
				Console.WriteLine("CONSONANT");
				break;
				case 'w':
				Console.WriteLine("CONSONANT");
				break;
				case 'x':
				Console.WriteLine("CONSONANT");
				break;
				case 'y':
				Console.WriteLine("CONSONANT");
				break;
				case 'z':
				Console.WriteLine("CONSONANT");
				break;
				case 'A':
				Console.WriteLine("VOWEL");
				break;
				case 'B':
				Console.WriteLine("CONSONANT");
				break;
				case 'C':
				Console.WriteLine("CONSONANT");
				break;
				case 'D':
				Console.WriteLine("CONSONANT");
				break;
				case 'E':
				Console.WriteLine("VOWEL");
				break;
				case 'F':
				Console.WriteLine("CONSONANT");
				break;
				case 'G':
				Console.WriteLine("CONSONANT");
				break;
				case 'H':
				Console.WriteLine("CONSONANT");
				break;
				case 'I':
				Console.WriteLine("VOWEL");
				break;
				case 'J':
				Console.WriteLine("CONSONANT");
				break;
				case 'K':
				Console.WriteLine("CONSONANT");
				break;
				case 'L':
				Console.WriteLine("CONSONANT");
				break;
				case 'M':
				Console.WriteLine("CONSONANT");
				break;
				case 'N':
				Console.WriteLine("CONSONANT");
				break;
				case 'O':
				Console.WriteLine("VOWEL");
				break;
				case 'P':
				Console.WriteLine("CONSONANT");
				break;
				case 'Q':
				Console.WriteLine("CONSONANT");
				break;
				case 'R':
				Console.WriteLine("CONSONANT");
				break;
				case 'S':
				Console.WriteLine("CONSONANT");
				break;
				case 'T':
				Console.WriteLine("CONSONANT");
				break;
				case 'U':
				Console.WriteLine("VOWEL");
				break;
				case 'V':
				Console.WriteLine("CONSONANT");
				break;
				case 'W':
				Console.WriteLine("CONSONANT");
				break;
				case 'X':
				Console.WriteLine("CONSONANT");
				break;
				case 'Y':
				Console.WriteLine("CONSONANT");
				break;
				case 'Z':
				Console.WriteLine("CONSONANT");
				break;
				default:
				Console.WriteLine("Not a Letter");
				break;		
			}
			return letter;
		}
	}
}