﻿using System;

namespace iterating2Darray
{
	class Program
	{
		public static void Main(string[] args)
		{
			int row, col;
			
			Console.Write("Enter Row: ");
			row = Convert.ToInt32(Console.ReadLine());
			
			Console.Write("Enter Col: ");
			col = Convert.ToInt32(Console.ReadLine());
			
			Console.WriteLine("Input");
			int[,] arr = new int[row, col];
			int i = 0;
			int j = 0;
			for(; i < row; i++){
				for(j = 0; j < col; j++){
					Console.Write("Enter Element in Index [{0},{1}]: ", i,j); 
					arr[i, j] = Convert.ToInt32(Console.ReadLine());
				}
			}
			Console.WriteLine();
			Console.WriteLine("Output");
			
			foreach(int x in arr)
			{
				Console.Write(x + "    ");
			}
		
			Console.ReadKey(true);
		}
	}
}