﻿using System;

namespace PerfTask6
{
    class Program
    {
        static void Main(string[] args)
        {
            int n, count = 0;
            int s = 0;
            int max = 0; 
            int  min = 2147483647; 
            string pls = "";
            double av;
			// startup
            Console.WriteLine("Input (+) integers to calculate some processes or (-) integers to terminate:");
            Console.WriteLine("----------------------------------------------------------------------------");
            Console.Write("Input positive integer to process or negative integer to terminate: "); n = Convert.ToInt32(Console.ReadLine());
            Console.Write("------------------------------------------------------------------");
          	Console.WriteLine();
			
            // complicated stuff
            
            while (n > 0 || n == 0)
            {
                if (n > 0)
                {
                    count++;
                    s += n;
                    pls += n;

                    if (min > n)
                    {
                        min = n;
                    }
                    if (max < n)
                    {
                        max = n;
                    }
                    Console.Write("Input positive integer to process or negative integer to terminate: ");
                    n = Convert.ToInt32(Console.ReadLine());
                     Console.Write("------------------------------------------------------------------");
                    Console.WriteLine();
                }
                else if (n == 0)
                {
                    Console.Write("Error: input must be positive or negative, not ZERO! try again... ");
                    Console.Write("\nInput positive integer to process or negative integer to terminate: ");
                    n = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine();
                }
                
            }
            // results
            Console.WriteLine();
            Console.WriteLine("Your input is for termination. Here is the result below: ");
            Console.WriteLine("--------------------------------------------------------");
            Console.WriteLine("Number of positive integers is: {0}", count);
            Console.WriteLine("The maximum value is: {0}", max);
            Console.WriteLine("The minimum value is: {0}", min);
            Console.WriteLine("{0} = {1}", pls, s);
           						 double dvd = s;
            					double dvr = count;
           						 av = dvd / dvr;
           						 av = Math.Round(av, 2);
           						 Console.WriteLine("The average is {0}", av);

            Console.ReadKey(true);
        }
    }
}

