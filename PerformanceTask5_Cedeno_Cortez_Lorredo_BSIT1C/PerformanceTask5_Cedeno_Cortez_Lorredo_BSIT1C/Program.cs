﻿
using System;

namespace PerformanceTask5_Cedeno_Cortez_Lorredo_BSIT1C
{
	class Program
	{
		public static void Main(string[] args)
		{
			// variable
			int number;
         	int count;
         	//input
         	Console.WriteLine("1-3999");
            Console.Write("Enter a number: ");
            number = Convert.ToInt32(Console.ReadLine());
            //process & output
            if (number < 0 || number > 3999)
            {
            	Console.WriteLine("Out of Range");
            }
            else
            {
           		if (number >= 1000 && number < 4000)
           		{
           			count = number / 1000;
           			for (int i = 0; i < count; i++)
           			{
            			Console.Write("M");
          	 		}
            	}	
            	number = number % 1000;	
           		if (number >= 100)
           		{
           	 		count = number / 100;
           
           			if  ( count == 9 )
           			{
            			Console.Write("CM");
           			}
           			else if ( count >= 5 )
           			{
            			Console.Write("D");
            			for (int j = 0; j < count - 5; j++)
            			{
            				Console.Write("C");
            			}
           			}
           			else if ( count == 4)
           			{
           				Console.Write("CD");
           			}
           			else if ( count <= 3)
           			{
           				for (int k = 0; k < count; k++ )
           				{
           					Console.Write("C");
           				}
           			}
           		}	number = number % 100;
           
           		if ( number >= 10 && number < 100)
           		{	
           			count = number / 10;
           			if ( count == 9 )
           			{
           				Console.Write("XC");
           			}
           			else if ( count >= 5 )
           			{
           				Console.Write("L");
           				for (int l = 0; l < count - 5; l++)
           				{
           					Console.Write("X");
           				}	    
           			}
           			else if (count == 4)
           			{
           				Console.Write("XL");
           			}
           			else if (count <= 3)
           			{
           				for (int m = 0; m < 3; m++)
           				{
           					Console.Write("X");
           				}
           			}
          		}	number = number % 10;
           
           		if ( number >= 1 )
           		{
           			count = number;
           			if ( count == 9 )
           			{
           				Console.Write("IX");
           			}
           			else if ( count >= 5 )
           			{
           				Console.Write("V");
           				for (int n = 0; n < count - 5; n++)
           				{
           					Console.Write("I");
           				}
           			}
           			else if ( count == 4)
           			{
           				Console.Write("IV");
           			}
           			else if ( count <= 3)
           			{ 
           				for (int o = 0; o < count; o++)
           				{
           					Console.Write("I");
           				}
           			}
           		}
            }
			Console.ReadKey(true);
		}
	}
}