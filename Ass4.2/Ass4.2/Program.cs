﻿using System;

namespace Ass4._
{
	class Program
	{
		public static void Main(string[] args)
		{
			Console.Write("Enter # of Row: "); 
			int row = Convert.ToInt32(Console.ReadLine());
			Console.Write("Enter # of Column: ");
			int column = Convert.ToInt32(Console.ReadLine());
			Console.WriteLine();
			Console.WriteLine("Input");
			int[,] arr = new int[row,column];
			
			for(int i = 0; i<arr.GetLength(0); i++)
			{
				for(int j = 0; j<arr.GetLength(1); j++)
				{
					Console.Write(row " "+ column );
				}
				Console.WriteLine();
			}
			Console.ReadKey(true);
		}
	}
}