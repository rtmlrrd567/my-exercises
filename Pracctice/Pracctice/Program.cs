﻿using System; 
using System.Collections.Generic;

namespace Stack
{
    class Program
    {
        public static void Main(string[] args)
        {
            //Declaring Variables
            int num;
            int evencount = 0;
            int oddcount = 0;
            
            //Create a Stack
            Stack<int> allnum = new Stack<int>(); 
            Stack<int> even  = new Stack<int>();    
            Stack<int> odd = new Stack<int>();     
            
            Console.WriteLine("Enter 6 Even Numbers and Enter 6 odd numbers");
            Console.WriteLine("The system will not include if your input is more than 6 odd and even number.");
            Console.WriteLine();
            
            //For process
            for(int i = 0; i  != 12; i++)
            {
                Console.Write("Enter a Number: ");
                num = Convert.ToInt32(Console.ReadLine()); //To read every input numbers
                
                allnum.Push(num); //To store inputs to allnum stack
                
                //To push to odd stack & and pop if the count is greater than six
                if(num %2 == 0){
                    even.Push(num);
                    evencount++; //to increment the count of odd numbers
                    if(evencount > 6)
                    {
                        allnum.Pop();  //To pop the excess even or odd numbers
                        even.Pop();
                        Console.WriteLine("Even number count is greater than six!");
                        i--; //To decrement the value of variable 'i' 
                    }
                }
                
                //To push to odd stack & and pop if the count is greater than six
                if(num %2 == 1)
                {
                    odd.Push(num);
                    oddcount++; //to increment the count of odd numbers
                    if(oddcount > 6)
                    {
                        allnum.Pop(); //To pop the excess even or odd numbers
                        odd.Pop();
                        Console.WriteLine("Even number count is greater than six!");
                        i--; //To decrement the value of variable 'i' 
                    }
                }
            }
            
            //For Outputs
            Console.Write("\n All Numbers: ");
            foreach(int all in allnum)
            {
                Console.Write(all + " ");
            }
            
            Console.Write("\n Even Numbers: ");
            foreach(int ev in even)
            {
                if(ev %2 == 0)
                Console.Write(ev + " ");
            }
            
            Console.Write("\n Odd Numbers: ");
            foreach(int od in odd)
            {
                if(od %2 == 1)
                Console.Write(od + " ");
            }
            Console.ReadKey(true);
        }
    }
}
