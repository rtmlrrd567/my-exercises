﻿using System;

namespace OneD_ArraydisplayoutputtoTable
{
	class Program
	{
		public static void Main(string[] args)
		{
			int row, col;
			
			Console.Write("Enter # of Rows: ");
			row = Convert.ToInt32(Console.ReadLine());
			Console.Write("Enter # of Columns: ");
			col = Convert.ToInt32(Console.ReadLine());
			
			int [,] num = new int[row, col];
			
			Console.WriteLine("Input");
			
			for(int i = 0; i < row; i++){
				for(int j = 0; j < col; j++){
					Console.Write("Enter Element in Index [{0},{1}]: ", i, j);
					num[i, j] = Convert.ToInt32(Console.ReadLine());
				}
			}
			for(int i = 0; i < row; i++){
				for(int j = 0; j < col; j++){
					Console.Write("{0}" + "\t", num[i, j] );
				}
				Console.WriteLine();
			}
			
			Console.ReadKey(true);
		}
	}
}