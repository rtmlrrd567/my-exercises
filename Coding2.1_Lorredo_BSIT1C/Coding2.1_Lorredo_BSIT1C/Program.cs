﻿using System;

namespace Coding2.__Lorredo_BSIT1C
{
	class Program
	{
		public static void Main(string[] args)
		{
			double fahrenheit;
			Console.Write("Enter a Fahrenheit: ");
			fahrenheit = Convert.ToDouble(Console.ReadLine());
			
			fahrenheitconverttocelsius(fahrenheit);
			
			Console.ReadKey(true);
		}
		
		public static double fahrenheitconverttocelsius(double fahrenheit)
		{
			double celsius;
			celsius = (fahrenheit-32) * 5 / 9 ;
			celsius = Math.Round(celsius, 2);
			Console.WriteLine("After Converting to Celsius the value is: {0}", celsius);
			return fahrenheit;
			
		}
	}
}