﻿/*
 * Created by SharpDevelop.
 * User: Evanice
 * Date: 5/31/2022
 * Time: 1:03 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace dasda
{
	class Program
	{
		public static void Main(string[] args)
		{
			Console.Write("Enter a String: "); string str = Console.ReadLine();
			Console.Write("Enter a Char value you want to Find: "); string char1 = Console.ReadLine();
			char char2 = Convert.ToChar(char1);
			
			bool b1 = str.Contains(char1);
			int count = 0;
			if(b1 == true)
			{
				Console.WriteLine("The Char '{0}' can be found in", char1);
				foreach(char i in str)
				{
					if(i == char2)
					{
						Console.WriteLine("Index: " + count);
					}
					count++;	
				}
			}
			Console.ReadKey(true);
		}
	}
}