﻿using System;

namespace Coding1.__Bait_Lorredo_Olimba_BSIT1C
{
	class Program
	{
		public static void Main(string[] args)
		{
			int n;
			
      		Console.Write("Enter a Number: ");   
      		n = Convert.ToInt32(Console.ReadLine());
			Console.WriteLine();
      	
      		if (n == 0)
      		{
      			Console.WriteLine("ZERO - Not Odd or Even");
      		}
      		
      		else if (n < 0)
      		{
      			Console.WriteLine("OUT OF RANGE");
      		}
			
      		else
      		{
      			Console.WriteLine("The Even Numbers from {0} to 1", n);
      			Console.WriteLine();
      			do
      			{
      				if (n % 2 == 0)
					Console.WriteLine(n);
      				n--;
      			}
      			while(n >= 1);
      		}
      		
      		Console.ReadKey(true);
		}
	}
}