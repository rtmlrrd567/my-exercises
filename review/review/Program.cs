﻿using System;
using System.Collections.Generic;

namespace review
{
	class Program
	{
		public static void Main(string[] args)
		{
			int s, odd, even;
			Stack<int> mystack = new Stack<int>();
			for(int i = 0; i < 10; i++)
			{
				Console.Write("Enter a Number: "); s = Convert.ToInt32(Console.ReadLine());
				mystack.Push(s);
			}
			
			Console.WriteLine();
			Console.WriteLine("Input Number");
			foreach(int u in mystack)
			{
				Console.Write(u + " ");
			}
			Console.WriteLine();
			Console.WriteLine("Odd Numbers");
			foreach(int u in mystack)
			{
				if(u%2 == 1)
				{
					Console.Write(u + " ");
				}
			}
			Console.WriteLine();
			Console.WriteLine("Even Numbers");
			foreach(int u in mystack)
			{
				if(u%2 == 0)
				{
					Console.Write(u + " ");
				}
			}
			
			
			
			
			
			Console.ReadKey(true);
		}
	}
}