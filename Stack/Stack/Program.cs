﻿using System; 
using System.Collections.Generic;

namespace Stack
{
    class Program
    {
        public static void Main(string[] args)
        {
            //Declaring Variables
            int num;
            
            //Create a Stack
            Stack<int> evenodd = new Stack<int>(); //Stack for all numbers
            Stack<int> even  = new Stack<int>();     //Stack for even numbers
            Stack<int> odd = new Stack<int>();      //Stack for odd numbers    
            
            //for instructions
            Console.WriteLine("Enter 6 Even Numbers and Enter 6 odd numbers");
            Console.WriteLine("If your input contains more than six even and odd number, the system will not incorporate it. \n");
            
            //For process
            for(int i = 0; i < 12; i++)
            {
                Console.Write("Enter a Number: ");
                num = Convert.ToInt32(Console.ReadLine()); //To read every input numbers
                
                evenodd.Push(num); //For storing the entered numbers to evenodd stack
                
                //Code that push the entered even number to the even stack and pop if the  entered even number exceed to 6
                if(num %2 == 0){
                    even.Push(num);
                    if(even.Count > 6)
                    {
                        evenodd.Pop();  //To pop the excess even or odd numbers
                        even.Pop();
                        Console.WriteLine("Even Number Exceed!!!");
                        i--; //To decrement the value of variable 'i' to stop it from reaching out to its condition
                    }
                }
                
                


//Code that push the entered odd number to the odd stack and pop if the the entered odd number exceed to 6
                if(num %2 == 1)
                {
                    odd.Push(num);
                    if(odd.Count > 6)
                    {
                        evenodd.Pop(); //To pop the excess even or odd numbers
                        odd.Pop();
                        Console.WriteLine("Odd Number Exceed!!!");
                        i--; //To decrement the value of variable 'i' to stop it from reaching out to its condition
                    }
                }
            }
            
            //For evenodd stack output
            Console.Write("\n All Numbers: ");
            foreach(int allnum in evenodd)
            {
                Console.Write(allnum + ", ");
            }
            
            //For even stack output
            Console.Write("\n Even Numbers: ");
            foreach(int e in even)
            {
                if(e %2 == 0)
                Console.Write(e + ", ");
            }
            
            //For odd stack output
            Console.Write("\n Odd Numbers: ");
            foreach(int o in odd)
            {
                if(o %2 == 1)
                Console.Write(o + ", ");
            }
            Console.ReadKey(true);
        }
    }
}

