﻿using System;

namespace Coding4.__Lorredo_Cedeno_Cortez_BSIT1C
{
	class Program
	{
		public static void Main(string[] args)
		{
			//variable
			double L1, L2, L3;
			double P;
			
			//input
			Console.Write("Enter the Length of the 1st Side: ");L1 = Convert.ToDouble(Console.ReadLine());
			Console.Write("Enter the Length of the 2nd Side: ");L2 = Convert.ToDouble(Console.ReadLine());
			Console.Write("Enter the Length of the 3rd Side: ");L3 = Convert.ToDouble(Console.ReadLine());
			
			//process
			P = L1 + L2 + L3;
			P = Math.Round(P, 2);
			
			//output
			Console.WriteLine("The Perimeter of the Triangle is : {0}", P);
			
			Console.ReadKey(true);
		}
	}
}