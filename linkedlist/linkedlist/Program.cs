﻿using System;
using System.Collections.Generic;

namespace linkedlist
{
    class Program
    {
        public static void Main(string[] args)
        {
            //* Create a Linkedlist named MOVIES
            LinkedList<string> MOVIES = new LinkedList<string>();
            
            //1.Add 'Avatar' as the first item in the list. 
            MOVIES.AddFirst("Avatar");
            Console.WriteLine("'Avatar' as the first item in the list:");
            foreach(string str in MOVIES) Console.WriteLine(str);
            Console.WriteLine(); //for spacing
            
            /*2.Create a LinkedListNode<String> named titanic with
            the value “Titanic” and add it to the last position in the list.*/
            LinkedListNode<string> Titanic = new LinkedListNode<string>("Titanic");
            MOVIES.AddLast(Titanic);
            Console.WriteLine("After adding 'Titanic':");
            foreach(string str in MOVIES) Console.WriteLine(str);
            Console.WriteLine();
            
            /*3.Add “Star Wars: The Force Awakens after Titanic using
            the “Titanic” node as an anchor.*/
            MOVIES.AddAfter(Titanic, "Star Wars: The Force Awakens");
            Console.WriteLine("After adding Star Wars: The Force Awakens:");
            foreach(string str in MOVIES) Console.WriteLine(str);
            Console.WriteLine();
            
            /*4.Remove the “Star Wars: The Force Awakens and Titanic
            on your list*/
            MOVIES.Remove(Titanic);
            MOVIES.Remove("Star Wars: The Force Awakens");
            Console.WriteLine("After Removing 'Star Wars: The Force Awakens' and 'Titanic':");
            foreach(string str in MOVIES) Console.WriteLine(str);
            Console.WriteLine();
            //5.Count the number of movies in the list
            Console.WriteLine("MOVIE/s count:");
            Console.WriteLine(MOVIES.Count);
            Console.WriteLine();
            
            //6.Find if the movie avatar is in the list
            if(MOVIES.Contains("Avatar")) Console.WriteLine("Element Founded!");
            else Console.WriteLine("Element not Found!");
            
            Console.ReadKey(true);
        }
    }
}

