﻿using System;

namespace while_loop_example
{
	class Program
	{
		public static void Main(string[] args)
		{
			int i = 0;
				while (i <= 3)
			{
				Console.WriteLine(i);
				i++;
				
			}
			
			Console.ReadKey(true);
		}
	}
}