﻿using System;

namespace PerformanceTask6_Lorredo_BSIT1C
{
	class Program
	{
		public static void Main(string[] args)
		{
			int number = 0;
			int maximum = 0;
			int minimum = 999999999;
			int total = 0;
			string w = "";
			int x = 0;
			double average;
			
			Console.WriteLine("Input (+) intergers to calculate some processes or (-) integers to terminate:");
			Console.WriteLine("-----------------------------------------------------------------------------");
		
			Console.Write("Input positive integer to process or negative integer to terminate: ");
				number = Convert.ToInt32(Console.ReadLine());
				
			while (number > 0 || number == 0)
			{
				if (number > 0)
				{
					
					total++;
					x += number;
                    w += number;
				
			 		
					if ( number > maximum)
					{
						maximum = number;
					}
				
			
					if (number < minimum)
					{
						minimum = number;
					}
					Console.Write("Input positive integer to process or negative integer to terminate: ");
					number = Convert.ToInt32(Console.ReadLine());
				}
				
				else if (number == 0)
				{
					Console.Write("Error: input must be positive or negative, not ZERO! try again... ");
                    Console.Write("\nInput positive integer to process or negative integer to terminate: ");
                    number = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine();
					
				}
				w = number > 0 ? w + " + " : w;	
			}
			
			Console.WriteLine("Your Input is for termination. Here is your result below:");
			Console.WriteLine("Number of positive integer input is: " + total );
			Console.WriteLine("The maximum value is: " + maximum); 
			Console.WriteLine("The minimum value is: " + minimum); 
			Console.WriteLine("{0} = {1}", w, x);
           	double y = x;
            double z = total;
         	average = y / z;
           	average = Math.Round(average, 2);
           	Console.WriteLine("The average is {0}", average);
			

	
			
			
			
			Console.ReadKey(true);
		}
	}
}