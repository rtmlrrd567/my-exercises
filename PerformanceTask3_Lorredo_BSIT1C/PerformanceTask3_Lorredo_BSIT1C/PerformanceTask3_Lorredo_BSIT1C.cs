﻿using System;
using System.Collections;

namespace PerformanceTask3_Lorredo_BSIT1C
{
	class Program
	{
		public static void Main(string[] args)
		{
			Queue myqueue = new Queue();
		Program:
			Console.WriteLine("QUEUE");
			Console.WriteLine();
			Console.WriteLine("Press 1 - Enqueue \nPress 2 - Dequeue \nPress 3 - Peek \nPress 4 - No. of Element \nPress 5 - View All \nPress 6 - Clear \nPress 7 - Exit\n");
			Console.Write("Enter a Number: ");
			int Press = Convert.ToInt32(Console.ReadLine());
			Console.WriteLine();
				if(Press == 1)
				{
					Console.Write("Enter a Value: "); 
					myqueue.Enqueue(Console.ReadLine());
					Console.Clear();
					goto Program;
				}
				if(Press == 2)
				{
					if(myqueue.Count == 0)
					{
						Console.WriteLine("No Elements to Dequeue");
					}
					else
					{
						Console.WriteLine("The First Element is Dequeued: {0}", myqueue.Dequeue());
					}
					Console.ReadKey(true);
					Console.Clear();
					goto Program;
				}
				if(Press == 3)
				{
					if(myqueue.Count == 0)
					{
						Console.WriteLine("First Element: None");
					}
					else
					{
						Console.WriteLine("First Element: {0}", myqueue.Peek());
					}
					Console.ReadKey(true);
					Console.Clear();
					goto Program;
				}
				if(Press == 4)
				{
					Console.WriteLine("Total No. Element: {0} ", myqueue.Count);
					Console.ReadKey(true);
					Console.Clear();
					goto Program;
				}
				if(Press == 5)
				{
					int count = myqueue.Count;
					int difference = count-count+1;
					foreach(var elements in myqueue)
					{
						Console.WriteLine("Element #{0} : "+ elements, difference);
						difference++;
					}
					Console.ReadKey(true);
					Console.Clear();
					goto Program;
				}
				if(Press == 6)
				{
					myqueue.Clear();
					Console.Clear();
					goto Program;
				}
				if(Press == 7)
				{
					Console.Write("Are You Sure?(Y/N): ");
					string answer = Console.ReadLine();
					Console.WriteLine();
					if(answer == "N" || answer == "n")
					{
						Console.Clear();
						goto Program;
					}
					else
					{
						Console.WriteLine("Thank You for Queuing!!!");
					}
				}
				if(Press < 1 || Press > 7)
				{
					Console.WriteLine();
					Console.WriteLine("Wrong Input!!!");
					Console.WriteLine();
					Console.Write("Do you want to Quit?(Y/N): ");
					string answer = Console.ReadLine();
					Console.WriteLine();
					if(answer == "N" || answer == "n")
					{
						Console.Clear();
						goto Program;
					}
					else 
					{
						Console.WriteLine("Thank You for Queuing!!!");
					}
				}
			Console.ReadKey(true);
		}
	}
}