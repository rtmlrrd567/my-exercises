﻿
using System;

namespace Coding5.__Baldeo_Cedeno_Javier_Lorredo_Mapula
{
	class Program
	{
		public static void Main(string[] args)
		{
			//variabble
			int n1, n2;
			//input
			Console.Write("Enter 1st Number: ");
			n1 = Convert.ToInt32(Console.ReadLine());
			
			Console.Write("Enter 2nd Number: ");
			n2 = Convert.ToInt32(Console.ReadLine());
			//process & output
			if (n1 > 0 && n2 > 0)
			{
				Console.WriteLine("BOTH POSITIVE");
			}
			else if (n1 < 0 && n2 < 0)
			{
				Console.WriteLine("BOTH NEGATIVE");
			}
			else if (n1 == 0 && n2 == 0)
			{
				Console.WriteLine("BOTH ZERO");
			}
			else if (n1 >= 0 && n2 <= 0 || n1 <= 0 && n2 >= 0)
			{
				Console.WriteLine("DIFFERENT SIGN");
			}
			
			
			Console.ReadKey(true);
		}
	}
}