﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4._1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.Write("How many words do You want to Enter: ");
            int al = Convert.ToInt32(Console.ReadLine());
            int n = 1;
            string[] s = new string [al];
            for (int i = 0; i < al; i++,n++)
            {
                Console.Write("Enter word #{0}: ",n);
                s[i] = Console.ReadLine();
            }
            Console.WriteLine("\nPress 1 to sortin Ascending Order:\nor\nPress 2 to sortin Descending Order:\n");
            char o = Convert.ToChar(Console.Read());

            Array.Sort(s);
            switch(o)
            {
                case '1':
                    Console.Clear();
                    Console.WriteLine("Words in ascending order:\n");

                    foreach (string x in s)
                    {
                        Console.WriteLine(x);
                    }
                    break;
                case '2':
                    Array.Reverse(s);
                    Console.Clear();
                    Console.WriteLine("Words in ascending order:\n");
                    foreach (string x in s)
                    {
                        Console.WriteLine(x);
                    }
                    break;
                default:
                    Console.WriteLine("invalid");
                    break;
            }
            Console.ReadKey(true);

        }
    }
}