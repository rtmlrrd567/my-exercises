﻿using System;

namespace Coding4.__Lorredo_Cedeno_Cortez_BSIT1C
{
	class Program
	{
		public static void Main(string[] args)
		{
			//variables
			double Pi = 3.14;
			double R, H;
			double Area;
			
			//input
			Console.Write("Enter Radius: ");R = Convert.ToDouble(Console.ReadLine());
			Console.Write("Enter Height: ");H = Convert.ToDouble(Console.ReadLine());
			
			//process
			Area = Pi * (R*R) * H / 3;
			Area = Math.Round(Area, 2);
			
			//ouput
			Console.WriteLine("The Volume of the Cone is: {0}", Area);
			
			Console.ReadKey(true);
		}
	}
}