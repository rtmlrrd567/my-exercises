﻿
using System;

namespace Coding6._
{
	class Program
	{
		public static void Main(string[] args)
		{
			double num;
			double twice;
			Console.Write("Enter a Number: ");
			num = Convert.ToDouble(Console.ReadLine());
			do
			{
				twice = num+num;
				if (num != twice)
				{
					Console.Write("Enter a Number: ");
					num = Convert.ToDouble(Console.ReadLine());
				}
				else
				{
					Console.WriteLine("");
				}
			}while (num != twice);
			Console.Clear();
			Console.Write("Double");
			Console.ReadKey(true);
		}
	}
}