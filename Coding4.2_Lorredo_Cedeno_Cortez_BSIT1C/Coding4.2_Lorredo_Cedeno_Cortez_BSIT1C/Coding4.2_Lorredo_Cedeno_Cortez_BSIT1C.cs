﻿
using System;

namespace Coding4.__Lorredo_Cedeno_Cortez_BSIT1C
{
	class Program
	{
		public static void Main(string[] args)
		{	
			//variable
			int number;
			int remainder;
			string ans;
			
			//input 
			Console.Write("Enter a Number: ");
			number = Convert.ToInt32(Console.ReadLine());
			
			//process
			remainder = number % 2;
			ans = remainder == 1 ?"is an Odd number":"is an Even number";
			
 			//output
 			Console.WriteLine(" {0} {1}", number, ans);
			Console.ReadKey(true);
		}
	}
}