﻿using System;

namespace PerformanceTask2_Lorredo_BSIT1C
{
	class Program
	{
		public static void Main(string[] args)
		{
			Console.Write("Enter a String: "); string str = Console.ReadLine();
			Console.Write("Enter a Char Value you want to Find: "); string char1 = Console.ReadLine();
			char char2 = Convert.ToChar(char1);
			Console.WriteLine();
			bool b = str.Contains(char1);
			int count = 0;
			if(b == true)
			{
				Console.WriteLine("The Char '{0}' can be found in", char1);
				Console.WriteLine();
				foreach(char character in str)
				{
					if(character == char2)
					{
						Console.WriteLine("Index: " + count);
					}
					count++;	
				}
			}
			else
			{
				Console.WriteLine("Character Not Existing");
			}
			Console.ReadKey(true);	  
		}
	}
}