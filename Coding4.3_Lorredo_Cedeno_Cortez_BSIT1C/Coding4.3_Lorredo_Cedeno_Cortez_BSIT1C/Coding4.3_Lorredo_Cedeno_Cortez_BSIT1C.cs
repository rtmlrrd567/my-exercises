﻿using System;

namespace Coding4.__Lorredo_Cedeno_Cortez_BSIT1C
{
	class Program
	{
		public static void Main(string[] args)
		{
			//variables
			double Pi = 3.14;
			double R;
			double Area;
			
			//input
			Console.Write("Enter Radius: ");
			R = Convert.ToDouble(Console.ReadLine());
			
			//process
			Area = 4 * (Pi) * (R*R);
			Area = Math.Round(Area, 2);
			
			//output
			Console.WriteLine("The Surface Area of Sphere is: {0}", Area);
			
			Console.ReadKey(true);
		}
	}
}