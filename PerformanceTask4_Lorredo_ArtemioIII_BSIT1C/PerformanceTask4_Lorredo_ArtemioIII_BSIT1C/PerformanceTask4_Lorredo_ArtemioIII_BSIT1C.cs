﻿using System;

namespace PerformanceTask4_Lorredo_ArtemioIII_BSIT1C
{
	class Program
	{
		public static void Main(string[] args)
		{
			// variables
			int U = 5;
			int A;
			int T;
			int F;
			
			// input
			Console.WriteLine("TOKENS");
			Console.WriteLine("BUY 10 get 2 for FREE!!!");
			Console.WriteLine("P5.00 per Token");
			Console.WriteLine();
			Console.Write("How many Tokens do you want to buy?");
			T = Convert.ToInt32(Console.ReadLine());
			Console.WriteLine();
			// process
			U *= T;
			A = (T / 10) * 2 + T;
			F = (T / 10) * 2;
			Console.WriteLine();
			Console.WriteLine();
			// output
			Console.WriteLine("You will Pay P {0}.00", U);
			Console.WriteLine("You will Acquire {0} Token(s)", A);
			Console.WriteLine("Paid Token(s):{0}", T);
			Console.WriteLine("Free Token(s):{0}", F);
			
			Console.ReadKey(true);
		}
	}
}