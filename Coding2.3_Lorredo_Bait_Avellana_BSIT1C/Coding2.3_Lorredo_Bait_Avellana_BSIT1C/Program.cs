﻿using System;

namespace coding2_3
{
    class program
    {
        public static void Main(string[] args)
        {
            int number;
            Console.Write("Enter a Number: ");
            number = Convert.ToInt32(Console.ReadLine());

            factorialofanumber(number);

       		Console.ReadKey(true);
       	}
        public static int factorialofanumber(int number)
        {
        	long factorial = 1;
        	
        	if(number < 0)
        	{
        		Console.WriteLine("Out of Range");
        	}
        	else if(number > 20)
        	{
        		Console.WriteLine("Out of Range");
        	}
        	else
        	{
        		for(int i = number; i >= 2; i--)
        		{
        			factorial = factorial * i;
        		}
        		Console.WriteLine("{0} ! = {1}", number, factorial);
        	}
        	return number;
        }
        
	}
}
