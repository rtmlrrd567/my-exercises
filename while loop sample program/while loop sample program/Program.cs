﻿using System;

namespace while_loop_sample_program
{
	class Program
	{
		public static void Main(string[] args)
		{
			string answer;
			string correctanswer = "Carabao";
			int i = 5;
			while (i > 0)
			{
				Console.WriteLine("Life: " + i);
				Console.WriteLine("Who is the National Animal of the Philippines?");
				Console.Write("Your Answer: "); answer = (Console.ReadLine());
			
				if (answer == correctanswer)
				{
					Console.WriteLine("You Won!");
					
					
					
				}
				else i--;
				
				if (i == 0) Console.WriteLine("You Lost!");
			}
			
			Console.ReadKey(true);
		}
	}
}