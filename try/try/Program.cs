﻿using System;

namespace Coding3.__Cedeño_Cortez_Lorredo__BSIT1C.cs
{
	class Program
	{
		public static void Main(string[] args)
		{
			//variable
			int number;
			int product;
			//input
			Console.Write("What is the number? ");
			number = Convert.ToInt32(Console.ReadLine());
			Console.WriteLine("Multiplication Table from 1 to " + number);
			//process
			for (int i = 1; i<=10; i++)
			{
				for (int j = 1; j<=number; j++)
				{
					product = j * i;
					//output
					Console.WriteLine("{0} x {1} = {2} ", j, i, product);
				}
			}
			
		
			
			Console.ReadKey(true);
		}
	}
}