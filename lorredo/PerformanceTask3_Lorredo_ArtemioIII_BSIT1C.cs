﻿using System;

namespace PerformanceTask3_Lorredo_ArtemioIII_BSIT1C.cs
{
	class Program
	{
		public static void Main(string[] args)
		{
			// Input
			Console.Write("Enter Last Name:");string LastName = Console.ReadLine();
			Console.Write("Enter First Name:");string FirstName = Console.ReadLine();
			Console.Write("Enter Middle Name:");string MiddleName = Console.ReadLine(); 
			Console.Write("Enter Birth Month(1-12):");int BirthMonth = Convert.ToInt32(Console.ReadLine());
			Console.Write("Enter Birth Date(1-31):");int BirthDate = Convert.ToInt32(Console.ReadLine());
			Console.Write("Enter Brth Year:");int BirthYear = Convert.ToInt32(Console.ReadLine());
			Console.Write("Enter Birth Place:");string BirthPlace = Console.ReadLine();
			Console.Write("Enter Gender (M/F):");string Gender = Console.ReadLine();
			Console.Write("Enter Civil Status (Single/Married):");string CivilStatus = Console.ReadLine();
			Console.Write("Enter Home Adress:");string HomeAdress = Console.ReadLine();
			Console.Write("Enter School Graduated:");string SchoolGraduated = Console.ReadLine();
			Console.Write("Enter Strand(ICT/GAS/STEM/etc.):");string Strand = Console.ReadLine();
			Console.Write("Enter 1st Grading Period Grade:");double FirstGradingGrade = Convert.ToDouble(Console.ReadLine());
			Console.Write("Enter 2nd Grading Period Grade:");double SecondGradingGrade = Convert.ToDouble(Console.ReadLine());
			Console.Write("Enter 3rd Grading Period Grade:");double ThirdGradingGrade = Convert.ToDouble(Console.ReadLine());
			Console.Write("Enter 4th Grading Period Grade:");double FourthGradingGrade = Convert.ToDouble(Console.ReadLine());
			Console.WriteLine();
			
			// Process
			int difference;
			double sum, ave;
			sum = FirstGradingGrade+SecondGradingGrade+ThirdGradingGrade+FourthGradingGrade;
			ave = sum/4; Console.ReadLine();
			difference = 2020 - BirthYear;
			
			// Output
			Console.WriteLine("****************************PERSONAL INFORMATION***************************");
			Console.WriteLine("Name:  {0}   {1}  {2}", FirstName, MiddleName, LastName);
			Console.WriteLine("***************************************************************************");
			Console.WriteLine("Birthday:  {0}   {1}  {2}				Age(in 2020): {3}",BirthMonth, BirthDate, BirthYear, difference);
			Console.WriteLine("***************************************************************************");
			Console.WriteLine("Birthplace: {0}", BirthPlace);
			Console.WriteLine("***************************************************************************");
			Console.WriteLine("Gender: {0}					Civil Status: {1}", Gender, CivilStatus);
			Console.WriteLine("***************************************************************************");
			Console.WriteLine("Home Address: {0}", HomeAdress);
			Console.WriteLine("***************************************************************************");
			Console.WriteLine("School Graduated: {0}", SchoolGraduated);
			Console.WriteLine("***************************************************************************");
			Console.WriteLine("Strand: {0}", Strand);
			Console.WriteLine("***************************************************************************");
			Console.WriteLine("Grades:");
			Console.WriteLine("***************************************************************************");
			Console.WriteLine("FirstGrading: {0}", FirstGradingGrade);
			Console.WriteLine("SecondGrading: {0}", SecondGradingGrade);
			Console.WriteLine("ThirdGrading: {0}", ThirdGradingGrade);
			Console.WriteLine("FourthGrading: {0}			General Average: {1}", FourthGradingGrade, ave);
			Console.WriteLine("***************************************************************************");
			
			Console.ReadKey(true);
		}
	}
}