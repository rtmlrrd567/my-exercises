﻿using System;
namespace Performanc_Task___3
{
	class Program
	{
		public static void Main(string[] args)
		{
			
			// Input
			
			Console.Write("Enter Last Name:");string LastName = Console.ReadLine();
			Console.Write("Enter First Name:");string FirstName = Console.ReadLine();
			Console.Write("Enter Middle Name:");string MiddleName = Console.ReadLine();
			Console.Write("Enter Birth Month(1-12):");int BirthMonth = Convert.ToInt32(Console.ReadLine());
			Console.Write("Enter Birth Date(1-31):");int BirthDate = Convert.ToInt32(Console.ReadLine());
			Console.Write("Enter Brth Year:");int BirthYear = Convert.ToInt32(Console.ReadLine());
			Console.Write("Enter Birth Place:");string BirthPlace = Console.ReadLine();
			Console.Write("Enter Gender (M/F):");string Gender = Console.ReadLine();
			Console.Write("Enter Civil Status (Single/Married):");string CivilStatus = Console.ReadLine();
			Console.Write("Enter Home Adress:");string HomeAdress = Console.ReadLine();
			Console.Write("Enter School Graduated:");string SchoolGraduated = Console.ReadLine();
			Console.Write("Enter Strand(ICT/GAS/STEM/etc.):");string Strand = Console.ReadLine();
			Console.Write("Enter 1st Grading Grade:");int FirstGradingGrade = Convert.ToInt32(Console.ReadLine());
			Console.Write("Enter 2nd Grading Grade:");int SecondGradingGrade = Convert.ToInt32(Console.ReadLine());
			Console.Write("Enter 3rd Grading Grade:");int ThirdGradingGrade = Convert.ToInt32(Console.ReadLine());
			Console.Write("Enter 4th Grading Grade:");int FourthGradingGrade = Convert.ToInt32(Console.ReadLine());
			Console.WriteLine();
			
			// Process
			
			int sum = FirstGradingGrade+SecondGradingGrade+ThirdGradingGrade+FourthGradingGrade;
			double ave = sum/4; Console.ReadLine();
			int difference = 2020-(BirthYear); Console.ReadLine();
			
			// Output
			
			Console.WriteLine("****************************PERSONAL INFORMATION***************************");
			Console.WriteLine("Name:  {0}   {1}  {2}",FirstName,MiddleName,LastName); 
			Console.WriteLine("***************************************************************************");
			Console.WriteLine("Birthday:  {0}   {1}  {2}",BirthMonth,BirthDate,BirthYear); Console.WriteLine("Age(in 2020): {0}",difference);
			Console.WriteLine("***************************************************************************");
			Console.WriteLine("Birthplace: {0}",BirthPlace);
			Console.WriteLine("***************************************************************************");
			Console.WriteLine("Gender: {0}",Gender); Console.WriteLine("Civil Status: {0}",CivilStatus);
			Console.WriteLine("***************************************************************************");
			Console.WriteLine("Home Adress: {0}",HomeAdress);
			Console.WriteLine("***************************************************************************");
			Console.WriteLine("School Graduated: {0}",SchoolGraduated);
			Console.WriteLine("***************************************************************************");
			Console.WriteLine("Strand: {0}",Strand);
			Console.WriteLine("***************************************************************************");
			Console.WriteLine("Grades:");
			Console.WriteLine("***************************************************************************");
			Console.WriteLine("FirstGrading: {0}",FirstGradingGrade);
			Console.WriteLine("SecondGrading: {0}",SecondGradingGrade);
			Console.WriteLine("ThirdGrading: {0}",ThirdGradingGrade);
			Console.WriteLine("FourthGrading: {0}",FourthGradingGrade); Console.WriteLine("Gen. ave.:{0}",ave);
			Console.WriteLine("***************************************************************************");
			
			Console.ReadKey(true);
		}
	}
}