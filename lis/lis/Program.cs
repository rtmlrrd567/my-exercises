﻿using System;

using System.Collections.Generic;

namespace list
{
	class Program
	{
		public static void Main(string[] args)
		{
			List<int> Mylist = new List <int>();
			
			Mylist.Add(100);
			Mylist.Add(5);
			Mylist.Add(75);
			Mylist.Add(50);
			
			List<string> Mylist1 = new List<string>();
			Mylist1.Add("Pogi ako");
			Mylist1.Add("Gwapo ako");
			Mylist1.Insert(1, "Sorry Pogi Lang");
			
			List<string> Mylist2 = new List<string>();
			Mylist2.Add("Artem Pogi");
			Mylist2.Add("Artem");
			
			Mylist1.InsertRange(0, Mylist2);
		
			foreach(var list in Mylist1){
				Console.WriteLine(list);
			}
			
			Console.ReadKey(true);
		}
	}
}