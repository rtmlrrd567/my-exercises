﻿/*
 * Created by SharpDevelop.
 * User: Evanice
 * Date: 7/16/2022
 * Time: 4:06 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Coding5.__Lorredo_BSIT1C
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class MainForm : Form
	{
		public MainForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		
		void Button1Click(object sender, EventArgs e)
		{
			BackColor = Color.Red;
			button1.Visible = false;
			button2.Visible = true;
			button3.Visible = true;
			button4.Visible = true;
			button5.Visible = true;
			button6.Visible = true;
			button7.Visible = true;
			label1.Text = "RED";
		}
		void Button2Click(object sender, EventArgs e)
		{
			BackColor = Color.Orange;
			button1.Visible = true;
			button2.Visible = false;
			button3.Visible = true;
			button4.Visible = true;
			button5.Visible = true;
			button6.Visible = true;
			button7.Visible = true;
			label1.Text = "ORANGE";
		}
		void Button3Click(object sender, EventArgs e)
		{
			BackColor = Color.Yellow;
			button1.Visible = true;
			button2.Visible = true;
			button3.Visible = false;
			button4.Visible = true;
			button5.Visible = true;
			button6.Visible = true;
			button7.Visible = true;
			label1.Text = "YELLOW";
		}
		void Button4Click(object sender, EventArgs e)
		{
			BackColor = Color.Green;
			button1.Visible = true;
			button2.Visible = true;
			button3.Visible = true;
			button4.Visible = false;
			button5.Visible = true;
			button6.Visible = true;
			button7.Visible = true;
			label1.Text = "GREEN";
		}
		void Button5Click(object sender, EventArgs e)
		{
			BackColor = Color.Blue;
			button1.Visible = true;
			button2.Visible = true;
			button3.Visible = true;
			button4.Visible = true;
			button5.Visible = false;
			button6.Visible = true;
			button7.Visible = true;
			label1.Text = "BLUE";
		}
		void Button6Click(object sender, EventArgs e)
		{
			BackColor = Color.Indigo;
			button1.Visible = true;
			button2.Visible = true;
			button3.Visible = true;
			button4.Visible = true;
			button5.Visible = true;
			button6.Visible = false;
			button7.Visible = true;
			label1.Text = "INDIGO";
		}
		void Button7Click(object sender, EventArgs e)
		{
			BackColor = Color.Violet;
			button1.Visible = true;
			button2.Visible = true;
			button3.Visible = true;
			button4.Visible = true;
			button5.Visible = true;
			button6.Visible = true;
			button7.Visible = false;
			label1.Text = "VIOLET";
		}
	}
}
